var z = require('zero-fill')
  , n = require('numbro')

module.exports = function container (get, set, clear) {
  var notify = get('lib.notify')
  var control_line = false;
  var buySignalCount = 0;
  return {
    name: 'macd_cross',
    description: 'Buy/sell on MACD/signal cross',

    getOptions: function () {
      this.option('period', 'period length', String, '1h')
      this.option('min_periods', 'min. number of history periods', Number, 52)
      this.option('ema_short_period', 'number of periods for the shorter EMA', Number, 5)
      this.option('ema_long_period', 'number of periods for the longer EMA', Number, 10)
      this.option('ema_control_period', 'number of periods for the control EMA', Number, 0)
      this.option('signal_period', 'number of periods for the signal EMA', Number, 5)
      this.option('up_trend_threshold', 'threshold to trigger a buy signal', Number, 0)
      this.option('down_trend_threshold', 'threshold to trigger a sold signal', Number, 0)
      this.option('overbought_rsi_periods', 'number of periods for overbought RSI', Number, 14)
      this.option('overbought_rsi', 'sold when RSI exceeds this value', Number, 70)
      this.option('profit_target_pct', 'set profit target this % over last buy price', Number, .1)
    },

    calculate: function (s) {
      s.options.rsi_periods = s.options.overbought_rsi_periods
      get('lib.rsi')(s, 'overbought_rsi', s.options.overbought_rsi_periods)
      if (!s.in_preroll && s.period.overbought_rsi >= s.options.overbought_rsi && !s.overbought) {
        s.overbought = true
        if (s.options.mode === 'sim' && s.options.verbose) console.log(('\noverbought at ' + s.period.overbought_rsi + ' RSI, preparing to sold\n').cyan)
      }

      // compture MACD
      get('lib.ema')(s, 'ema_short', s.options.ema_short_period)
      get('lib.ema')(s, 'ema_long', s.options.ema_long_period)
      if (s.period.ema_short && s.period.ema_long) {
        s.period.macd = (s.period.ema_short - s.period.ema_long)
        get('lib.ema')(s, 'signal', s.options.signal_period, 'macd')
        if (s.period.signal) {
          s.period.macd_histogram = s.period.macd - s.period.signal
        }
      }

      // compute control line
      if(!s.options.ema_control_period == 0) {
        get('lib.ema')(s, 'ema_control', s.options.ema_control_period)
      }
    },

    onPeriod: function (s, cb) {
      // if (!s.in_preroll && typeof s.period.overbought_rsi === 'number') {
      //   if (s.overbought) {
      //     s.overbought = false
      //     s.trend = 'overbought'
      //     s.signal = 'sold'
      //     return cb()
      //   }
      // }

      // determine target
      s.price_sell_target = s.last_buy_price * (1 + (s.options.profit_target_pct / 100))

      s.sell_target_reached = isNaN(s.last_buy_price)
      if(!s.sell_target_reached) {
        s.sell_target_reached = (s.price_sell_target < s.period.close)      
      }

      if(s.options.profit_target_pct == 0) {
        s.sell_target_reached = true // don't use sell target
      }

      control_line = s.period.close > s.period.ema_control
      if(s.options.ema_control_period == 0) {
        control_line = true // dont use a control line
      }

      var orig_capital = s.orig_capital || s.start_capital
      var consolidated = n(s.balance.currency).add(n(s.period.close).multiply(s.balance.asset)).value()
      var profit = (consolidated - orig_capital) / orig_capital

      if (typeof s.period.macd === 'number' && typeof s.lookback[0].macd === 'number' && typeof s.period.signal === 'number' && typeof s.lookback[0].signal === 'number') {
        if ((s.period.macd > s.period.signal) && (s.lookback[0].macd < s.lookback[0].signal) && control_line && buySignalCount == 0 && (s.period.overbought_rsi >= 50 && s.period.overbought_rsi <= 80)) {
          s.signal = 'buy';
          buySignalCount++;         
        } else if ((s.period.macd < s.period.signal) && (s.lookback[0].macd > s.lookback[0].signal) && s.sell_target_reached) {
          s.signal = 'sell';
          buySignalCount = 0;
          if(profit >= 0.05) {
            notify.pushMessage('PROFIT', 'I reached ' + (profit * 100) + '% profit trading ' + s.product.asset + '-' + s.product.currency)
          }
        } else {
          s.signal = null;  // hold
        }
      }
      cb()
    },

    onReport: function (s) {
      var cols = []
      var macdColor = 'grey'
        if (s.period.macd > s.period.signal) {
          macdColor = 'green'
        }
        else {
          macdColor = 'red'
        }
        var controlColor = 'gray'
        if(control_line) {
          controlColor = 'green'
        }
        else {
          controlColor = 'red'
        }
        var targetColor = 'grey'
        if (s.sell_target_reached) {
          targetColor = 'green'
        }
        else {
          targetColor = 'red'
        }
      cols.push('Signal: ' + z(8, n(s.period.signal).format('00.0000'), ' ').yellow + '; Macd: ' + z(8, n(s.period.macd).format('00.0000'), ' ')[macdColor] + '; Control: ' + z(8, n(s.period.ema_control).format('00.0000'), ' ')[controlColor] + '; Sell target: ' + z(8, n(s.price_sell_target).format('00.0000'), ' ')[targetColor])
      cols.push()
      return cols
    }
  }
}